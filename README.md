# Service Name

## Description

This section should be used to provide a brief description of the service.

## Setup

Install the following applications:

- [Git](http://git-scm.com/)
- [Node.js](http://nodejs.org/) (with NPM)

The service is a NodeJS application. Its dependencies are installed and managed using NPM.

- `git clone <repository-url>` this repository
- change into the new directory
- `npm install`

## Contribution Guidelines

- Writing tests
- Code review
- Other guidelines

## NPM Commands

The table below lists the commands which can be run in the following format:

`npm run [command]`

| Command    | Description                                                                                                     |
| ---------- | --------------------------------------------------------------------------------------------------------------- |
| `build-ts` | Perform a single typescript build                                                                               |
| `watch-ts` | Start a process to watch the source code for changes, then perform a typescript build when a change is detected |
| `test`     | Run project unit tests                                                                                          |
| `lint`     | Run the linting tool to check for errors                                                                        |
| `lint-fix` | Run the linting tool to check for (and attempt to automatically fix) error                                      |
