import { makeLog4JSLogger } from './logger-log4js';
import { Logger } from './types';

export { Logger };
export const logger: Logger = makeLog4JSLogger();
