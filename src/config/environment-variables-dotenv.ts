import dotenv, { DotenvConfigOutput } from 'dotenv';
import { EnvironmentVariables } from './types';
import { Logger } from '../external-interfaces/logger';

class DotEnvConfig implements EnvironmentVariables {
  #envFound: DotenvConfigOutput;

  constructor(logger: Logger) {
    const path = process.cwd();
    this.#envFound = dotenv.config({ path: path + '/.env' });
    if (this.#envFound.error) {
      const msg = `Error loading environment variables; could not find a .env file in ${path}`;
      logger.error(msg);
      throw new Error(msg);
    }
  }

  serverPort(): number {
    return parseInt(<string>process.env['SERVER_PORT'], 10);
  }
}

export function makeDotEnvConfig(logger: Logger): DotEnvConfig {
  return new DotEnvConfig(logger);
}
