export interface EnvironmentVariables {
  serverPort(): number;
}
