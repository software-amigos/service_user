import { environment } from './';

describe('Environment variables', () => {
  it('should specify the server port', () => {
    expect(environment.serverPort()).toBeDefined();
    expect(environment.serverPort()).toBeGreaterThanOrEqual(0);
    expect(environment.serverPort()).toBeLessThanOrEqual(65535);
  });
});
