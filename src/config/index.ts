import { EnvironmentVariables } from './types';
import { makeDotEnvConfig } from './environment-variables-dotenv';
import { logger } from '../external-interfaces/logger';

export { EnvironmentVariables };
export const environment: EnvironmentVariables = makeDotEnvConfig(logger);
