import { logger } from './external-interfaces/logger';
import { environment } from './config/';
import express from 'express';

async function startServer() {
  const app = express();
  const port = environment.serverPort();

  app.listen(port, (e) => {
    if (e) throw e;
  });

  logger.info(`Server listening on port ${port}`);
}

try {
  startServer();
} catch (e) {
  logger.fatal(e);
  process.exit(1);
}

process.on('SIGINT', () => {
  console.log('App aborted...');
  process.exit();
});
